
select json_object  (
                        'id' is role_group.role_group_id,
                        'label' is role_group.reference,
                        'roles' is json_arrayagg(
                            json_object(
                                'id' is role.role_id,
                                'label' is role.reference
                            ) order by role.reference returning varchar2
                        )
                    ) as role_group_json
from bci_services.role_group
inner join bci_services.role on role.role_group_id = role_group.role_group_id
where role_group.is_deleted = 0 and role.is_deleted = 0
group by role_group.reference, role_group.role_group_id
order by role_group.reference, role_group.role_group_id;

select json_arrayagg(
            json_object(
                'id' is stateorprovince_id,
                'label' is reference
            ) order by reference returning varchar2
       ) as stateorprovince_json
from COMMON.stateorprovince
where country_id = 12;

select json_object  (
                        'id' is parent_category.category_id,
                        'label' is parent_category.reference,
                        'subSectors' is json_arrayagg(
                            json_object(
                                'id' is category.category_id,
                                'label' is category.reference
                            ) order by category.reference returning varchar2
                        )
                    ) as category_json
from common.category_group
inner join common.category on category.category_id = category_group.category_id
inner join common.category parent_category on parent_category.category_id = category_group.parent_category_id
where category_group.country_id = 12 and category.is_deleted = 0 and parent_category.is_deleted = 0
group by parent_category.reference, parent_category.category_id
order by parent_category.reference, parent_category.category_id;

select json_arrayagg(
            json_object(
                'id' is development_type_id,
                'label' is reference
            ) order by reference returning varchar2
       ) as development_type_json
from bci_services.research_development_type
where is_deleted = 0;
