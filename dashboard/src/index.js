import * as React from 'react';
import ReactDOM from 'react-dom';
import { StyledEngineProvider } from '@mui/material/styles';
import RelationshipMapping from './RelationshipMapping';

ReactDOM.render(
  <StyledEngineProvider injectFirst>
    <RelationshipMapping />
  </StyledEngineProvider>,
  document.querySelector("#root")
);