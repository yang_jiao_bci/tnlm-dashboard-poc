import * as React from 'react';
import {
  BrowserRouter as Router,
  useLocation
} from "react-router-dom";
import PropTypes from 'prop-types';
import { alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import Slider from '@mui/material/Slider';
import Stack from '@mui/material/Stack';
import LoadingButton from '@mui/lab/LoadingButton';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Toolbar from '@mui/material/Toolbar';
import Checkbox from '@mui/material/Checkbox';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { visuallyHidden } from '@mui/utils';
import { Pie } from '@nivo/pie';

export default function RelationshipMapping() {
  return (
    <Router>
      <RelationshipMappingBox />
    </Router>
  );
}

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function RelationshipMappingBox() {
  const id = parseInt(useQuery().get("id"));
  // const loadReportsApiUrl = 'https://yangtest.free.beeceptor.com/api/load-relationship-mapping';
  // const filterReportsApiUrl = 'https://yangtest.free.beeceptor.com/api/filter-relationship-mapping';
  const loadReportsApiUrl = 'http://127.0.0.1:8000/api/load-relationship-mapping';
  const filterReportsApiUrl = 'http://127.0.0.1:8000/api/filter-relationship-mapping';
  const dense = false;
  const rowsPerPage = 5;
  const minValueDistance = 1;

  const [error, setError] = React.useState(null);
  const [isLoaded, setIsLoaded] = React.useState(false);
  const [roleGroups, setRoleGroups] = React.useState([]);
  const [minYear, setMinYear] = React.useState(null);
  const [maxYear, setMaxYear] = React.useState(null);
  const [states, setStates] = React.useState([]);
  const [sectors, setSectors] = React.useState([]);
  const [minValue, setMinValue] = React.useState(null);
  const [maxValue, setMaxValue] = React.useState(null);
  const [developmentTypes, setDevelopmentTypes] = React.useState([]);
  const [commonProjects, setCommonProjects] = React.useState([]);
  const [marketShare, setMarketShare] = React.useState([]);
  const [selectedRoleGroup, setSelectedRoleGroup] = React.useState([]);
  const [roleOptions, setRoleOptions] = React.useState([]);
  const [selectedRole, setSelectedRole] = React.useState([]);
  const [selectedYear, setSelectedYear] = React.useState([]);
  const [selectedState, setSelectedState] = React.useState([]);
  const [selectedsector, setSelectedSector] = React.useState([]);
  const [subSectorOptions, setSubSectorOptions] = React.useState([]);
  const [selectedSubSector, setSelectedSubSector] = React.useState([]);
  const [selectedValue, setSelectedValue] = React.useState([]);
  const [selectedDevelopmentType, setSelectedDevelopmentType] = React.useState([]);
  const [filteredCommonProjects, setFilteredCommonProjects] = React.useState([]);
  const [filteredMarketShare, setFilteredMarketShare] = React.useState([]);
  const [applying, setApplying] = React.useState(false);
  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('value');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);

  const headCells = [
    {
      id: 'name',
      numeric: false,
      disablePadding: true,
      label: 'Partner',
    },
    {
      id: 'value',
      numeric: true,
      disablePadding: false,
      label: 'Number of Ongoing Projects',
    },
  ];

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const EnhancedTableHead = (props) => {
    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
      props;
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              color="primary"
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{
                'aria-label': 'select all partners',
              }}
            />
          </TableCell>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align={headCell.numeric ? 'right' : 'left'}
              padding={headCell.disablePadding ? 'none' : 'normal'}
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <Box component="span" sx={visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </Box>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }

  EnhancedTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
  };

  const EnhancedTableToolbar = (props) => {
    const { numSelected } = props;

    return (
      <Toolbar
        sx={{
          pl: { sm: 2 },
          pr: { xs: 1, sm: 1 },
          ...(numSelected > 0 && {
            bgcolor: (theme) =>
              alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
          }),
        }}
      >
        {numSelected > 0 ? (
          <Typography
            sx={{ flex: '1 1 80%' }}
            color="inherit"
            variant="subtitle1"
            component="div"
          >
            {numSelected} selected
          </Typography>
        ) : (
          <Typography
            sx={{ flex: '1 1 100%' }}
            variant="h6"
            id="tableTitle"
            component="div"
          >
            Common projects with partners
          </Typography>
        )}

        {numSelected > 0 ? (
          <LoadingButton
            size="medium"
            color="error"
            loadingIndicator="Linking..."
            variant="contained">
            Link partners
          </LoadingButton>
        ) : (
          <></>
        )}
      </Toolbar>
    );
  };

  EnhancedTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const isSelected = (name) => selected.indexOf(name) !== -1;

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - filteredCommonProjects.length) : 0;

  React.useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ branchOfficeId: id })
    };

    fetch(loadReportsApiUrl, requestOptions)
      .then(res => res.json())
      .then(
        (result) => {
          let filterMinYear = parseInt(result.filters.minPublishedYear);
          let filterMaxYear = parseInt(result.filters.maxPublishedYear);
          if (filterMaxYear === filterMinYear) {
            filterMaxYear += 1;
          }
          let filterMinValue = parseInt(result.filters.minValue);
          let filterMaxValue = parseInt(result.filters.maxValue);
          if (filterMaxValue === filterMinValue) {
            filterMaxValue += 1;
          }
          const reportsCommonProjects = result.reports.commonProjects;
          reportsCommonProjects.forEach(e => e.value = parseInt(e.value));
          setMinYear(filterMinYear);
          setMaxYear(filterMaxYear);
          setMinValue(filterMinValue);
          setMaxValue(filterMaxValue);
          setStates(result.filters.states);
          setDevelopmentTypes(result.filters.developmentTypes);
          setRoleGroups(result.filters.roleGroups);
          setSectors(result.filters.sectors)
          setCommonProjects(reportsCommonProjects);
          setMarketShare(result.reports.marketShare);
          setFilteredCommonProjects(reportsCommonProjects);
          setFilteredMarketShare(result.reports.marketShare);
          setSelectedYear([filterMinYear, filterMaxYear]);
          setSelectedValue([filterMinValue, filterMaxValue])
          setIsLoaded(true);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [id]);

  if (Number.isNaN(id)) {
    return <div>Invalid Id</div>;
  } else if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <Box sx={{ flexGrow: 1 }}>
        <Typography variant="h4" component="h1" gutterBottom>
          Relationship Mapping
        </Typography>
        <Grid container spacing={4}>
          <Grid item xs={12} md={6} lg={3}>
            <Autocomplete
              multiple
              filterSelectedOptions
              id="filter-partner-role-group"
              options={roleGroups}
              value={selectedRoleGroup}
              onChange={(event, newValue) => {
                const newRoleOptions = newValue.map(e => e.roles).reduce((previous, current) => previous.concat(current), []);
                const newRoleOptionIdSet = newRoleOptions.reduce((previous, current) => { previous[current.id] = null; return previous }, {});
                const newSelectedRole = selectedRole.filter(e => e.id in newRoleOptionIdSet);
                setSelectedRoleGroup(newValue);
                setRoleOptions(newRoleOptions);
                setSelectedRole(newSelectedRole)
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Partner Role Group(s)"
                  placeholder="Please pick a role group"
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6} lg={3}>
            <Autocomplete
              multiple
              filterSelectedOptions
              id="filter-partner-role"
              options={roleOptions}
              groupBy={(option) => option.parent_label}
              value={selectedRole}
              onChange={(event, newValue) => {
                setSelectedRole(newValue);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Partner Role(s)"
                  placeholder="Please pick a role"
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6} lg={3}>
            <Stack direction="row" spacing={2}>
              <Typography variant="subtitle1" component="div" gutterBottom>
                Year
              </Typography>
              <Slider
                id="filter-year"
                min={minYear}
                max={maxYear}
                step={1}
                value={selectedYear}
                onChange={(event, newValue) => {
                  setSelectedYear(newValue);
                }}
                valueLabelDisplay="on"
              />
            </Stack>
          </Grid>
          <Grid item xs={12} md={6} lg={3}>
            <Autocomplete
              multiple
              filterSelectedOptions
              id="filter-state"
              options={states}
              value={selectedState}
              onChange={(event, newValue) => {
                setSelectedState(newValue);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="State(s)"
                  placeholder="Please pick a state"
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6} lg={3}>
            <Autocomplete
              multiple
              filterSelectedOptions
              id="filter-sector"
              options={sectors}
              value={selectedsector}
              onChange={(event, newValue) => {
                const newSubSectorOptions = newValue.map(e => e.subSectors).reduce((previous, current) => previous.concat(current), []);
                const newSubSectorOptionIdSet = newSubSectorOptions.reduce((previous, current) => { previous[current.id] = null; return previous }, {});
                const newSelectedSubSector = selectedSubSector.filter(e => e.id in newSubSectorOptionIdSet);
                setSelectedSector(newValue);
                setSubSectorOptions(newSubSectorOptions);
                setSelectedSubSector(newSelectedSubSector);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Sector(s)"
                  placeholder="Please pick a sector"
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6} lg={3}>
            <Autocomplete
              multiple
              filterSelectedOptions
              id="filter-sub-sector"
              options={subSectorOptions}
              groupBy={(option) => option.parent_label}
              value={selectedSubSector}
              onChange={(event, newValue) => {
                setSelectedSubSector(newValue);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Sub-sector(s)"
                  placeholder="Please pick a sub-sector"
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={6} lg={3}>
            <Stack direction="row" spacing={2}>
              <Typography variant="subtitle1" component="div" gutterBottom>
                Value
              </Typography>
              <Slider
                disableSwap
                id="filter-value"
                min={minValue}
                max={maxValue}
                step={1}
                value={selectedValue}
                onChange={(event, newValue, activeThumb) => {
                  if (!Array.isArray(newValue)) {
                    return;
                  }

                  if (newValue[1] - newValue[0] < minValueDistance) {
                    if (activeThumb === 0) {
                      const clamped = Math.min(newValue[0], 100 - minValueDistance);
                      setSelectedValue([clamped, clamped + minValueDistance]);
                    } else {
                      const clamped = Math.max(newValue[1], minValueDistance);
                      setSelectedValue([clamped - minValueDistance, clamped]);
                    }
                  } else {
                    setSelectedValue(newValue);
                  }
                }}
                valueLabelDisplay="on"
                valueLabelFormat={value => `${value}M`}
              />
            </Stack>
          </Grid>
          <Grid item xs={12} md={6} lg={3}>
            <Autocomplete
              multiple
              filterSelectedOptions
              id="filter-development-type"
              options={developmentTypes}
              value={selectedDevelopmentType}
              onChange={(event, newValue) => {
                setSelectedDevelopmentType(newValue);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Development type(s)"
                  placeholder="Please pick a development type"
                />
              )}
            />
          </Grid>
          <Grid item container justifyContent="center">
            <Stack direction="row" spacing={2}>
              <LoadingButton
                variant="outlined"
                size="large"
                loading={applying}
                loadingIndicator="Applying..."
                onClick={async () => {
                  setApplying(true);
                  const requestOptions = {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                      branchOfficeId: id,
                      filters: {
                        minPublishedYear: selectedYear[0],
                        maxPublishedYear: selectedYear[1],
                        minValue: selectedValue[0],
                        maxValue: selectedValue[1],
                        states: selectedState.map(e => e.id),
                        developmentTypes: selectedDevelopmentType.map(e => e.id),
                        roles: selectedRole.map(e => e.id),
                        subSectors: selectedSubSector.map(e => e.id),
                      },
                    }),
                  };
                  try {
                    const response = await fetch(filterReportsApiUrl, requestOptions);
                    const responseData = await response.json();
                    setFilteredCommonProjects(responseData.reports.commonProjects);
                    setFilteredMarketShare(responseData.reports.marketShare);
                    setApplying(false);
                  } catch (e) {
                    setApplying(false);
                  }
                }}
              >
                Apply filters
              </LoadingButton>
              <Button
                variant="outlined"
                size="large"
                onClick={() => {
                  setSelectedRoleGroup([]);
                  setSelectedRole([]);
                  setSelectedYear([minYear, maxYear]);
                  setSelectedState([]);
                  setSelectedSector([]);
                  setSelectedSubSector([]);
                  setSelectedValue([minValue, maxValue]);
                  setSelectedDevelopmentType([]);
                  setFilteredCommonProjects(commonProjects);
                  setFilteredMarketShare(marketShare);
                }}
              >
                Reset filters
              </Button>
            </Stack>
          </Grid>
          <Grid item xs={12} md={12} lg={6}>
            <EnhancedTableToolbar numSelected={selected.length} />
            <TableContainer>
              <Table
                sx={{ minWidth: 750 }}
                aria-labelledby="tableTitle"
                size={dense ? 'small' : 'medium'}
              >
                <EnhancedTableHead
                  numSelected={selected.length}
                  order={order}
                  orderBy={orderBy}
                  onSelectAllClick={(event) => {
                    if (event.target.checked) {
                      const newSelecteds = filteredCommonProjects.map((n) => n.name);
                      setSelected(newSelecteds);
                      return;
                    }
                    setSelected([]);
                  }}
                  onRequestSort={(event, property) => {
                    const isAsc = orderBy === property && order === 'asc';
                    setOrder(isAsc ? 'desc' : 'asc');
                    setOrderBy(property);
                  }}
                  rowCount={filteredCommonProjects.length}
                />
                <TableBody>
                  {filteredCommonProjects.slice().sort(getComparator(order, orderBy))
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      const isItemSelected = isSelected(row.name);
                      const labelId = `enhanced-table-checkbox-${index}`;

                      return (
                        <TableRow
                          hover
                          onClick={(event) => handleClick(event, row.name)}
                          role="checkbox"
                          aria-checked={isItemSelected}
                          tabIndex={-1}
                          key={row.name}
                          selected={isItemSelected}
                        >
                          <TableCell padding="checkbox">
                            <Checkbox
                              color="primary"
                              checked={isItemSelected}
                              inputProps={{
                                'aria-labelledby': labelId,
                              }}
                            />
                          </TableCell>
                          <TableCell
                            component="th"
                            id={labelId}
                            scope="row"
                            padding="none"
                          >
                            {row.name}
                          </TableCell>
                          <TableCell align="right">
                            <Button
                              variant="contained"
                              target="_blank"
                              href={`https://staging.bci-tnlm.com/login?redirect=/main/common-project?company_id=${id}&partner_id=${row.id}&project_id=${row.list}`}
                              onClick={(e) => {
                                e.stopPropagation();
                              }}
                            >
                              {row.value}
                            </Button>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow
                      style={{
                        height: (dense ? 33 : 53) * emptyRows,
                      }}
                    >
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              component="div"
              rowsPerPageOptions={[5]}
              count={filteredCommonProjects.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={(event, newPage) => {
                setPage(newPage);
              }}
            />
          </Grid>
          <Grid item container xs={12} md={12} lg={6} justifyContent="center">
            <Pie
              data={filteredMarketShare}
              animate={true}
              isInteractive={true}
              activeOuterRadiusOffset={6}
              enableArcLabels={true}
              arcLabelsSkipAngle={18}
              enableArcLinkLabels={true}
              arcLinkLabelsSkipAngle={18}
              width={600}
              height={400}
              margin={{ top: 80, right: 60, bottom: 40, left: 60 }}
            />
          </Grid>
        </Grid>
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={applying}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Box>
    );
  }
}
