<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\DashboardController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/load-relationship-mapping', [DashboardController::class, 'loadRelationshipMapping']);
Route::post('/filter-relationship-mapping', [DashboardController::class, 'filterRelationshipMapping']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
