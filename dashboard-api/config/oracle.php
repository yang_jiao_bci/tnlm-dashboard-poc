<?php

return [
    'oracle' => [
        'driver'         => 'oracle',
        'tns'            => env('DB_TNS', ''),
        'host'           => env('DB_HOST', 'uat-db-lm-anz.bciaustralia.com'),
        'port'           => env('DB_PORT', '1521'),
        'database'       => env('DB_DATABASE', 'services'),
        'service_name'   => env('DB_SERVICENAME', 'services'),
        'username'       => env('DB_USERNAME', 'yang'),
        'password'       => env('DB_PASSWORD', 'Bci_2009'),
        'charset'        => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'         => env('DB_PREFIX', ''),
        'prefix_schema'  => env('DB_SCHEMA_PREFIX', ''),
        'edition'        => env('DB_EDITION', 'ora$base'),
        'server_version' => env('DB_SERVER_VERSION', '12c'),
        'dynamic'        => [],
    ],
];
