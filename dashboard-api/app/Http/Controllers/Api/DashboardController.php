<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    const QUERY_RELATIONSHIP_MAPPING_FILTER_VALUE_AND_YEAR = <<<SQL
            select min(mv_project.published_year) as min_published_year,
                    max(mv_project.published_year) as max_published_year,
                    floor(min(mv_project.value) / 1e6) as min_value,
                    ceil(max(mv_project.value) / 1e6) as max_value
            from yang.mv_project_role_branch_office
            inner join yang.mv_project_role_branch_office partner_relationship on partner_relationship.project_id = mv_project_role_branch_office.project_id
            inner join yang.mv_project on mv_project.project_id = partner_relationship.project_id
            where mv_project_role_branch_office.branch_office_id = :branch_office_id and partner_relationship.branch_office_id != :branch_office_id
            SQL;

    const QUERY_RELATIONSHIP_MAPPING_FILTER_STATE = <<<SQL
            select stateorprovince_id as id,
                reference as label
            from common.stateorprovince
            where stateorprovince_id in (
                select mv_project.stateorprovince_id
                from yang.mv_project_role_branch_office
                inner join yang.mv_project_role_branch_office partner_relationship on partner_relationship.project_id = mv_project_role_branch_office.project_id
                inner join yang.mv_project on mv_project.project_id = partner_relationship.project_id
                where mv_project_role_branch_office.branch_office_id = :branch_office_id and partner_relationship.branch_office_id != :branch_office_id
            )
            SQL;

    const QUERY_RELATIONSHIP_MAPPING_FILTER_DEVELOPMENT_TYPE = <<<SQL
            select development_type_id as id,
                   reference as label
            from bci_services.research_development_type
            where development_type_id in (
                select mv_project.development_type_id
                from yang.mv_project_role_branch_office
                inner join yang.mv_project_role_branch_office partner_relationship on partner_relationship.project_id = mv_project_role_branch_office.project_id
                inner join yang.mv_project on mv_project.project_id = partner_relationship.project_id
                where mv_project_role_branch_office.branch_office_id = :branch_office_id and partner_relationship.branch_office_id != :branch_office_id
            )
            order by label
            SQL;

    const QUERY_RELATIONSHIP_MAPPING_FILTER_ROLE_GROUP = <<<SQL
            select role_group.role_group_id as parent_id,
                   role_group.reference as parent_label,
                   role.role_id as child_id,
                   role.reference as child_label
            from bci_services.role_group
            inner join bci_services.role on role.role_group_id = role_group.role_group_id
            where role_group.is_deleted = 0 and role.is_deleted = 0 and role.role_id in (
                select partner_relationship.role_id
                from yang.mv_project_role_branch_office
                inner join yang.mv_project_role_branch_office partner_relationship on partner_relationship.project_id = mv_project_role_branch_office.project_id
                where mv_project_role_branch_office.branch_office_id = :branch_office_id and partner_relationship.branch_office_id != :branch_office_id
            )
            order by role_group.reference, role.reference
            SQL;

    const QUERY_RELATIONSHIP_MAPPING_FILTER_SECTOR = <<<SQL
            select parent_category.category_id as parent_id,
                   parent_category.reference as parent_label,
                   category.category_id as child_id,
                   category.reference as child_label
            from common.category_group
            inner join common.category on category.category_id = category_group.category_id
            inner join common.category parent_category on parent_category.category_id = category_group.parent_category_id
            where category_group.country_id = 12 and category.is_deleted = 0 and parent_category.is_deleted = 0 and category.category_id in (
                select mv_project_category.category_id
                from yang.mv_project_role_branch_office
                inner join yang.mv_project_role_branch_office partner_relationship on partner_relationship.project_id = mv_project_role_branch_office.project_id
                inner join yang.mv_project_category on mv_project_category.project_id = partner_relationship.project_id
                where mv_project_role_branch_office.branch_office_id = :branch_office_id and partner_relationship.branch_office_id != :branch_office_id
            )
            order by parent_category.reference, category.reference
            SQL;

    const QUERY_RELATIONSHIP_MAPPING_REPORT_COMMON_PROJECTS_NO_FILTER = <<<SQL
            with
            partner_branch_office_project_id as (
                select distinct
                    partner_relationship.branch_office_id,
                    mv_project.project_id
                from yang.mv_project_role_branch_office
                inner join yang.mv_project_role_branch_office partner_relationship on partner_relationship.project_id = mv_project_role_branch_office.project_id
                inner join yang.mv_project on mv_project.project_id = partner_relationship.project_id
                where mv_project_role_branch_office.branch_office_id = :branch_office_id and partner_relationship.branch_office_id != :branch_office_id
            ),
            partner_branch_office_aggregation as (
                select branch_office_id,
                    count(project_id) as number_of_ongoing_projects,
                    listagg(project_id, ',') within group (order by project_id) as list_of_ongoing_project_ids
                from partner_branch_office_project_id
                group by branch_office_id
            )
            select company_translation.name,
                partner_branch_office_aggregation.number_of_ongoing_projects as value,
                branch_office.branch_office_id as id,
                partner_branch_office_aggregation.list_of_ongoing_project_ids as list
            from partner_branch_office_aggregation
            inner join bci_services.branch_office on branch_office.branch_office_id = partner_branch_office_aggregation.branch_office_id and branch_office.is_deleted = 0
            inner join bci_services.company on company.company_id = branch_office.company_id and company.is_deleted = 0
            inner join bci_services.company_translation on company_translation.company_id = company.company_id and company_translation.language_id = 1
            order by number_of_ongoing_projects desc, name asc
            SQL;

    const QUERY_RELATIONSHIP_MAPPING_REPORT_MARKET_SHARE_NO_FILTER = <<<SQL
            with
            partner_branch_office_project_id as (
                select distinct
                    partner_relationship.branch_office_id,
                    mv_project.project_id
                from yang.mv_project_role_branch_office
                inner join yang.mv_project_role_branch_office partner_relationship on partner_relationship.project_id = mv_project_role_branch_office.project_id
                inner join yang.mv_project on mv_project.project_id = partner_relationship.project_id
                where mv_project_role_branch_office.branch_office_id = :branch_office_id and partner_relationship.branch_office_id != :branch_office_id and mv_project.status_id not in (170, 171)
            ),
            partner_branch_office_number_of_partnerships as (
                select distinct
                    branch_office_id,
                    count(project_id) over (partition by branch_office_id) as number_of_partnerships_per_branch_office,
                    count(*) over () as number_of_partnership_all
                from partner_branch_office_project_id
            ),
            partner_branch_office_rank_all as (
                select branch_office_id,
                    number_of_partnerships_per_branch_office,
                    number_of_partnership_all,
                    dense_rank() over (order by number_of_partnerships_per_branch_office desc, branch_office_id asc) as branch_office_rank
                from partner_branch_office_number_of_partnerships
            ),
            partner_branch_office_rank_top_n as (
                select branch_office_id,
                    round(number_of_partnerships_per_branch_office / number_of_partnership_all * 100) as market_share,
                    branch_office_rank
                from partner_branch_office_rank_all
                where branch_office_rank <= 5
            ),
            partner_branch_office_market_share as (
                select branch_office_id,
                    market_share,
                    branch_office_rank
                from partner_branch_office_rank_top_n
                union
                select 0 as branch_office_id,
                    100 - sum(market_share) as market_share,
                    6 as branch_office_rank
                from partner_branch_office_rank_top_n
            )
            select case when partner_branch_office_market_share.branch_office_id = 0 then 'Others' else company_translation.name end as id,
                case when partner_branch_office_market_share.branch_office_id = 0 then 'Others' else company_translation.name end as label,
                partner_branch_office_market_share.market_share as value
            from partner_branch_office_market_share
            left outer join bci_services.branch_office on branch_office.branch_office_id = partner_branch_office_market_share.branch_office_id and branch_office.is_deleted = 0
            left outer join bci_services.company on company.company_id = branch_office.company_id and company.is_deleted = 0
            left outer join bci_services.company_translation on company_translation.company_id = company.company_id and company_translation.language_id = 1
            where partner_branch_office_market_share.market_share > 0
            order by partner_branch_office_market_share.branch_office_rank
            SQL;

    public function loadRelationshipMapping(Request $request)
    {
        $requestBody = json_decode($request->getContent(), false);
        $branchOfficeId = $requestBody->branchOfficeId;
        $filters = new \stdClass;

        $bindings = [
            'branch_office_id' => $branchOfficeId
        ];

        $results = DB::select(DB::raw(DashboardController::QUERY_RELATIONSHIP_MAPPING_FILTER_VALUE_AND_YEAR), $bindings);
        $filters->minPublishedYear = $results[0]->min_published_year;
        $filters->maxPublishedYear = $results[0]->max_published_year;
        $filters->minValue = $results[0]->min_value;
        $filters->maxValue = $results[0]->max_value;

        $results = DB::select(DB::raw(DashboardController::QUERY_RELATIONSHIP_MAPPING_FILTER_STATE), $bindings);
        $filters->states = $results;

        $results = DB::select(DB::raw(DashboardController::QUERY_RELATIONSHIP_MAPPING_FILTER_DEVELOPMENT_TYPE), $bindings);
        $filters->developmentTypes = $results;

        $results = DB::select(DB::raw(DashboardController::QUERY_RELATIONSHIP_MAPPING_FILTER_ROLE_GROUP), $bindings);
        $filters->roleGroups = $this->normalize($results, 'roles');

        $results = DB::select(DB::raw(DashboardController::QUERY_RELATIONSHIP_MAPPING_FILTER_SECTOR), $bindings);
        $filters->sectors = $this->normalize($results, 'subSectors');

        $reports = new \stdClass;

        $results = DB::select(DB::raw(DashboardController::QUERY_RELATIONSHIP_MAPPING_REPORT_COMMON_PROJECTS_NO_FILTER), $bindings);
        $reports->commonProjects = $results;

        $results = DB::select(DB::raw(DashboardController::QUERY_RELATIONSHIP_MAPPING_REPORT_MARKET_SHARE_NO_FILTER), $bindings);
        $reports->marketShare = $results;

        $responseBody = [
            'filters' => $filters,
            'reports' => $reports
        ];
        return response()->json($responseBody);
    }

    public function filterRelationshipMapping(Request $request)
    {
        $requestBody = json_decode($request->getContent(), false);
        $branchOfficeId = $requestBody->branchOfficeId;
        $filters = $requestBody->filters;

        $queryFilters = 'where ';
        $bindings = [
            'branch_office_id' => $branchOfficeId
        ];

        if (isset($filters->minPublishedYear)) {
            $queryFilters .= 'mv_project.published_year >= :min_published_year and ';
            $bindings['min_published_year'] = $filters->minPublishedYear;
        }

        if (isset($filters->maxPublishedYear)) {
            $queryFilters .= 'mv_project.published_year <= :max_published_year and ';
            $bindings['max_published_year'] = $filters->maxPublishedYear;
        }

        if (isset($filters->minValue)) {
            $queryFilters .= 'mv_project.value >= :min_value * 1e6 and ';
            $bindings['min_value'] = $filters->minValue;
        }

        if (isset($filters->maxValue)) {
            $queryFilters .= 'mv_project.value <= :max_value * 1e6 and ';
            $bindings['max_value'] = $filters->maxValue;
        }

        if (isset($filters->states) and !empty($filters->states)) {
            $queryFilters .= "mv_project.stateorprovince_id in (select trim(regexp_substr(id_list, '[^,]+', 1, level)) from (select :stateorprovince_id_list as id_list from dual) connect by instr(id_list, ',', 1, level - 1) > 0) and ";
            $bindings['stateorprovince_id_list'] = implode(',', $filters->states);
        }

        if (isset($filters->developmentTypes) and !empty($filters->developmentTypes)) {
            $queryFilters .= "mv_project.development_type_id in (select trim(regexp_substr(id_list, '[^,]+', 1, level)) from (select :development_type_id_list as id_list from dual) connect by instr(id_list, ',', 1, level - 1) > 0) and ";
            $bindings['development_type_id_list'] = implode(',', $filters->developmentTypes);
        }

        if (isset($filters->roles) and !empty($filters->roles)) {
            $queryFilters .= "partner_relationship.role_id in (select trim(regexp_substr(id_list, '[^,]+', 1, level)) from (select :role_id_list as id_list from dual) connect by instr(id_list, ',', 1, level - 1) > 0) and ";
            $bindings['role_id_list'] = implode(',', $filters->roles);
        }

        if (isset($filters->subSectors) and !empty($filters->subSectors)) {
            $queryFilters .= "mv_project.project_id in (select project_id from yang.mv_project_category where category_id in (select trim(regexp_substr(id_list, '[^,]+', 1, level)) from (select :category_id_list as id_list from dual) connect by instr(id_list, ',', 1, level - 1) > 0)) and ";
            $bindings['category_id_list'] = implode(',', $filters->subSectors);
        }

        $reports = new \stdClass;

        // reports: common projects
        $results = DB::select(DB::raw(preg_replace('/where /', $queryFilters, DashboardController::QUERY_RELATIONSHIP_MAPPING_REPORT_COMMON_PROJECTS_NO_FILTER, 1)), $bindings);
        $reports->commonProjects = $results;

        // reports: market share
        $results = DB::select(DB::raw(preg_replace('/where /', $queryFilters, DashboardController::QUERY_RELATIONSHIP_MAPPING_REPORT_MARKET_SHARE_NO_FILTER, 1)), $bindings);
        $reports->marketShare = $results;

        $responseBody = [
            'reports' => $reports
        ];
        return response()->json($responseBody);
    }

    private function normalize(array $resultSet, string $childrenKey)
    {
        $normalized = [];
        $currentParent = null;
        $currentChildren = [];
        foreach ($resultSet as $row) {
            $currentChild = new \stdClass;
            $currentChild->id = $row->child_id;
            $currentChild->label = $row->child_label;
            $currentChild->parent_label = $row->parent_label;
            if (is_null($currentParent) or $currentParent->id != $row->parent_id) {
                if (!is_null($currentParent)) {
                    $currentParent->{$childrenKey} = $currentChildren;
                    $currentChildren = [];
                }
                $currentParent = new \stdClass;
                $currentParent->id = $row->parent_id;
                $currentParent->label = $row->parent_label;
                array_push($normalized, $currentParent);
            }
            array_push($currentChildren, $currentChild);
        }
        if (!is_null($currentParent)) {
            $currentParent->{$childrenKey} = $currentChildren;
        }
        return $normalized;
    }
}
